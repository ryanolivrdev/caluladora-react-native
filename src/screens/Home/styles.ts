import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
  homeContainer: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: 24,
    marginTop: 120,
    marginBottom: 10,
  },
  subtitle: {
    fontSize: 14,
    marginBottom: 20,
    marginTop: 20,
    color: '666666',
  },
  react: {
    color: "blue"
  },
  input: {
    borderRadius: 5,
    backgroundColor: '#FFF',
    width: 150,
    height: 40,
    paddingLeft: 20,
  },
  operationWrapper: {
    display: "flex",
    flexDirection: "row",
    marginTop: 20
  },
  textButton: {
    fontWeight: 'bold',
    fontSize: 24,
    color: 'white',
  },
  textButtonBlack: {
    fontWeight: 'bold',
    fontSize: 24,
    color: 'black',
  },
  button: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
    paddingTop: 16,
    paddingBottom: 16,
    paddingRight: 24,
    paddingLeft: 24,
    borderRadius: 15,
    backgroundColor: '#8257E5',
  },
  buttonRed: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
    paddingTop: 16,
    paddingBottom: 16,
    paddingRight: 24,
    paddingLeft: 24,
    borderRadius: 15,
    backgroundColor: 'red',
  },
  buttonYellow: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 20,
    paddingTop: 16,
    paddingBottom: 16,
    paddingRight: 24,
    paddingLeft: 24,
    borderRadius: 15,
    backgroundColor: 'yellow',
  }
})
