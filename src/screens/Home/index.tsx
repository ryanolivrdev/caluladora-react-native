import { useState } from "react";
import { Text, TextInput, TouchableOpacity, View } from "react-native";

import { styles } from "./styles";

export function Home() {
  const [counter, setCounter] = useState<number>(0);
  const [value, setValue] = useState({
    Text: "",
    firstNumber: "",
    secondNumber: "",
    operation: "",
  });

  function clear() {
    setCounter(0);
    setValue({
      Text: "",
      firstNumber: "",
      secondNumber: "",
      operation: "",
    });
  }

  function addOperation(operationChoice: string) {
    if (operationChoice === "√") {
      setValue((prev) => ({ ...prev, Text: operationChoice }));
      setValue((prev) => ({ ...prev, operation: operationChoice }));
      return;
    }

    if (counter === 0) {
      setValue((prev) => ({ ...prev, operation: operationChoice }));
      setValue((prev) => ({ ...prev, Text: prev.Text + operationChoice }));
      setCounter(1);
    }
  }

  function addNumber(InputText: string) {
    if (value.Text.length === 0) {
      setCounter(0);
    }

    if (value.operation === "√") {
      const number = InputText.slice(1, InputText.length);
      setValue((prev) => ({ ...prev, firstNumber: number }));
      setValue((prev) => ({ ...prev, Text: InputText }));

      return;
    }

    if (value.operation === "SEN") {
      const number = InputText.slice(3, InputText.length);
      setValue((prev) => ({ ...prev, firstNumber: number }));
      setValue((prev) => ({ ...prev, Text: InputText }));

      return;
    }

    if (value.operation === "COS") {
      const number = InputText.slice(3, InputText.length);
      setValue((prev) => ({ ...prev, firstNumber: number }));
      setValue((prev) => ({ ...prev, Text: InputText }));

      return;
    }

    if (value.operation === "TAN") {
      const number = InputText.slice(3, InputText.length);
      setValue((prev) => ({ ...prev, firstNumber: number }));
      setValue((prev) => ({ ...prev, Text: InputText }));

      return;
    }

    if (counter === 0) {
      setValue((prev) => ({ ...prev, firstNumber: InputText }));
      setValue((prev) => ({ ...prev, Text: InputText }));
    }

    if (counter === 1) {
      const number = InputText.slice(
        value.firstNumber.length + 1,
        InputText.length
      );
      setValue((prev) => ({ ...prev, secondNumber: number }));
      setValue((prev) => ({ ...prev, Text: InputText }));
    }
  }

  function operation() {
    switch (value.operation) {
      case "+":
        const sum = Number(value.firstNumber) + Number(value.secondNumber);
        setValue((prev) => ({
          ...prev,
          Text: String(sum),
          firstNumber: String(sum),
          secondNumber: "",
          operation: "",
        }));
        setCounter(0);
        break;
      case "-":
        const subtraction =
          Number(value.firstNumber) - Number(value.secondNumber);
        setValue((prev) => ({
          ...prev,
          Text: String(subtraction),
          firstNumber: String(subtraction),
          secondNumber: "",
          operation: "",
        }));
        setCounter(0);
        break;
      case "*":
        const multiplication =
          Number(value.firstNumber) * Number(value.secondNumber);
        setValue((prev) => ({
          ...prev,
          Text: String(multiplication),
          firstNumber: String(multiplication),
          secondNumber: "",
          operation: "",
        }));
        setCounter(0);
        break;
      case "/":
        const division = Number(value.firstNumber) / Number(value.secondNumber);
        setValue((prev) => ({
          ...prev,
          Text: String(division),
          firstNumber: String(division),
          secondNumber: "",
          operation: "",
        }));
        setCounter(0);
        break;
      case "√":
        const squareRoot = Math.sqrt(Number(value.firstNumber));
        setValue((prev) => ({
          ...prev,
          Text: String(squareRoot),
          firstNumber: String(squareRoot),
          secondNumber: "",
          operation: "",
        }));
        setCounter(0);
        break;
      case "x":
        const exponentBase = Math.pow(
          Number(value.firstNumber),
          Number(value.secondNumber)
        );
        setValue((prev) => ({
          ...prev,
          Text: String(exponentBase),
          firstNumber: String(exponentBase),
          secondNumber: "",
          operation: "",
        }));
        setCounter(0);
        break;
      case "SEN":
        const radSEN = (Number(value.firstNumber) * Math.PI) / 180;
        const sine = Math.sin(radSEN);
        setValue((prev) => ({
          ...prev,
          Text: String(sine),
          firstNumber: String(sine),
          secondNumber: "",
          operation: "",
        }));
        setCounter(0);
        break;
      case "COS":
        const radCOS = (Number(value.firstNumber) * Math.PI) / 180;
        const cosine = Math.cos(radCOS);
        setValue((prev) => ({
          ...prev,
          Text: String(cosine),
          firstNumber: String(cosine),
          secondNumber: "",
          operation: "",
        }));
        setCounter(0);
        break;
      case "TAN":
        const rad = (Number(value.firstNumber) * Math.PI) / 180;
        const tangent = Math.tan(rad);
        setValue((prev) => ({
          ...prev,
          Text: String(tangent),
          firstNumber: String(tangent),
          secondNumber: "",
          operation: "",
        }));
        setCounter(0);
        break;
      default:
        break;
    }
  }

  return (
    <View style={styles.homeContainer}>
      <Text style={styles.title}>
        Calculadora<Text style={styles.react}>.tsx</Text>
      </Text>

      <TextInput
        style={styles.input}
        value={value.Text}
        onChangeText={(text) => {
          addNumber(text);
        }}
        keyboardType="numeric"
        placeholder="Digite um numero"
        placeholderTextColor="#B2B2B2"
        returnKeyType="done"
      />

      <View style={styles.operationWrapper}>
        <TouchableOpacity style={styles.buttonRed} onPress={() => clear()}>
          <Text style={styles.textButton}>C</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.button}
          onPress={() => addOperation("+")}
        >
          <Text style={styles.textButton}>+</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => addOperation("-")}
        >
          <Text style={styles.textButton}>-</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => addOperation("*")}
        >
          <Text style={styles.textButton}>*</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => addOperation("/")}
        >
          <Text style={styles.textButton}>/</Text>
        </TouchableOpacity>
      </View>

      <View style={styles.operationWrapper}>
        <TouchableOpacity
          style={styles.button}
          onPress={() => addOperation("√")}
        >
          <Text style={styles.textButton}>√</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => addOperation("x")}
        >
          <Text style={styles.textButton}>x²</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => addOperation("SEN")}
        >
          <Text style={styles.textButton}>SEN</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => addOperation("COS")}
        >
          <Text style={styles.textButton}>COS</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={() => addOperation("TAN")}
        >
          <Text style={styles.textButton}>TAN</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.buttonYellow} onPress={operation}>
          <Text style={styles.textButtonBlack}>=</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}
